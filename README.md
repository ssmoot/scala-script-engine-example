# Overview

This is just an example of using the Java `ScriptEngineManager` with Scala.

The `src/main/scala/Main.scala` is your entry point. Just run `sbt run` to execute it.
 
`src/main/scala/Cow.scala` is base class for testing. `migrations/Moo.scala` is a file that should be evaluated by
the `ScriptEngine`.