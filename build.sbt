name := "scala-script-engine-example"

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.6"

scalacOptions ++= Seq(
    "-language:_",
    "-feature",
    "-unchecked",
    "-deprecation")

resolvers ++= Seq(
    "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/",
    "Maven Central"      at "https://repo1.maven.org/maven2",
    "Typesafe Releases"  at "https://repo.typesafe.com/typesafe/releases/",
    "Spray Repository"   at "http://repo.spray.io")

libraryDependencies ++= Seq(
    "com.wieck"           %% "couchdb-utils"  % "1.1.3-SNAPSHOT",
    "com.typesafe.akka"   %% "akka-actor"     % "2.3.6",
    "com.typesafe"         % "config"         % "1.2.1",
    "org.scala-lang"       % "scala-compiler" % "2.11.6",
    "org.scala-lang"       % "scala-library"  % "2.11.6")

// Required to be able to use the Scala ScriptEngine.eval()!
fork in ( Compile, run ) := true