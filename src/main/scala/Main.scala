import scala.io.Source
import scala.util.Try
import javax.script.{ScriptEngine, ScriptEngineManager}
import java.io.File
import com.wieck.couchdb.utils._
import com.wieck.couchdb.utils.actors.Overwatch
import akka.actor._
import scala.collection.JavaConversions._
import com.typesafe.config.ConfigFactory

object Main extends App {

  def using[T <: {def close() : Unit}, R](source: T)(block: T => R): R = {
    val result = Try(block(source))
    source.close()
    result.get
  }

  val defaultConfig = ConfigFactory.parseString(
    """
      |couchdb {
      |  hostname = "127.0.0.1:5984"
      |  username = ""
      |  password = ""
      |  database = "utils-test"
      |}
    """.stripMargin)

  val config = ConfigFactory.parseFile(new File("conf/private.conf")) // Look for a private.conf
    .withFallback(defaultConfig) // Or use defaultConfig
    .withFallback(ConfigFactory.load) // finally merge with reference.conf

  val configuration = Configuration(
    hostname    = config.getString("couchdb.hostname"),
    secure      = config.getBoolean("couchdb.secure"),
    username    = config.getString("couchdb.username"),
    password    = config.getString("couchdb.password"),
    database    = config.getString("couchdb.database"),
    storagePath = config.getString("couchdb.backup.path"),
    maxBufferSize = config.getInt("couchdb.maxBufferSize"))

  val system = ActorSystem()
  val overwatch = system.actorOf(Props(classOf[Overwatch], configuration), name = "overwatch")

  val engine: ScriptEngine = new ScriptEngineManager().getEngineByName("scala")
  val settings = engine.asInstanceOf[scala.tools.nsc.interpreter.IMain].settings
  settings.usejavacp.value = true

  engine.put("_overwatch: akka.actor.ActorRef", overwatch)
  engine.eval("implicit val i_overwatch: akka.actor.ActorRef = _overwatch")

  val migrationFiles = new File("migrations").listFiles().toList
  migrationFiles foreach { file =>

    using(Source.fromFile(file)) { source =>
      println(file.getName.toUpperCase + "\n")
      engine.eval(source.mkString)
                                 }

    overwatch ! Messages.Migrate(false)
  }
}