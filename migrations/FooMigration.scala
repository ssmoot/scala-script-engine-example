import com.wieck.couchdb.utils.Migration
import spray.json._

object FooMigration extends Migration(4) {

  migrate {
            case obj@docType("video") =>
              PUT(JsObject(obj.fields - "foo" - "bar"))

            case key(id @ "video-1007982044001", rev) =>
              DELETE(id, rev)

          }
}